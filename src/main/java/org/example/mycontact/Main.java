package org.example.mycontact;

import org.example.mycontact.dto.User;
import org.example.mycontact.factory.ContactRepositoryFactory;
import org.example.mycontact.menu.*;
import org.example.mycontact.person.Contact;
import org.example.mycontact.service.*;
import org.example.mycontact.utillits.MyContactRepositoryFactory;
import org.example.mycontact.utillits.MyProperties;
import org.example.mycontact.utillits.Validator;
import org.example.mycontact.utillits.contact.ContactValidator;
import org.example.mycontact.utillits.contact.EmailValidator;
import org.example.mycontact.utillits.contact.PhoneValidator;
import org.example.mycontact.view.*;

import java.net.http.HttpClient;
import java.util.List;
import java.util.Scanner;

public class Main {

    public static final String APPLICATION_MODE_KEY = "application.mode";

    public static void main(String[] args) {
        Validator<Contact> validator = new ContactValidator(List.of(new EmailValidator(), new PhoneValidator()));
        HttpClient httpClient = HttpClient.newBuilder().build();
        MyProperties myProperties = new MyProperties();
        MyContactRepositoryFactory myContactRepositoryFactory =
                new MyContactRepositoryFactory(myProperties,
                        httpClient,
                        validator);
        ContactRepositoryFactory contactRepositoryFactory = myContactRepositoryFactory
                .getContactRepositoryFactory(APPLICATION_MODE_KEY);
        Scanner scanner = new Scanner(System.in);
        View<Contact> view = new ViewContacts(scanner);
        ViewUser viewUser = new ViewUser(scanner);
        ContactService ContactService = contactRepositoryFactory.createService();
        ChoiceView choiceView = new ChoiceView(scanner);
        UsersService usersService = new UsersService(httpClient,
                "https://mag-contacts-api.herokuapp.com/users");
        AuthService authService = contactRepositoryFactory.createAuthService();
        NetworkView networkView = new ViewStatusNewUser(scanner,authService);
        MenuContact menuContact = new MenuContact(scanner, new Menu[]
                {new AddContact(view, ContactService),
                        new SortForSurname(ContactService, view),
                        new GetOnlyNumbers(ContactService, view),
                        new GetOnlyEmail(ContactService, view),
                        new GetContactPartOfName(ContactService, choiceView),
                        new GetByValue(ContactService, choiceView),
                        new DeletedContact(choiceView, ContactService)
                });
        MenuDataBaseUsers menuDataBaseUsers = new MenuDataBaseUsers(scanner,new Menu[]{
                new AddUserDataBase(ContactService,viewUser),
        new GetDataBaseUser(ContactService,viewUser),new DeleteUserDataBase(viewUser,ContactService)},authService);
        MenuApiUsers menuApiUsers = new MenuApiUsers(scanner, new Menu[]{
                new RegisterNewUser(networkView),
                new EntryUser(networkView),
                new GetUsers(usersService)
        },authService,menuDataBaseUsers) ;
        menuApiUsers.run();
        MenuNetwork menuNetwork = new MenuNetwork(scanner, new Menu[]{
                new RegisterNewUser(networkView),
                new EntryUser(networkView),
                new GetUsers(usersService)
        },authService,menuContact);
//        menuNetwork.run();
    }
}



