package org.example.mycontact.dto;

import lombok.Builder;
import lombok.Data;
import lombok.RequiredArgsConstructor;

@Data
@RequiredArgsConstructor
@Builder
public class User {
    private String login;
    private String password;
    private String date_born;

    public User(String login, String password, String date_born) {
        this.login = login;
        this.password = password;
        this.date_born = date_born;
    }

    public User(String login, String password) {
        this.login = login;
        this.password = password;
    }
}
