package org.example.mycontact.menu;

import lombok.RequiredArgsConstructor;
import org.example.mycontact.exception.DuplicateContactKeyException;
import org.example.mycontact.exception.InvalidInputException;
import org.example.mycontact.person.Contact;
import org.example.mycontact.service.ContactService;
import org.example.mycontact.view.View;

@RequiredArgsConstructor
public class AddContact implements Menu {
    private final View<Contact> view;
    private final ContactService<Contact> fileContactService;


    @Override
    public String getName() {
        return "Добавить контакт";
    }

    @Override
    public void execute() {
        try {
            Contact contact = view.readContact();
            fileContactService.save(contact);
        } catch (InvalidInputException e) {
            System.out.println("Invalid data");
        }catch (DuplicateContactKeyException e){
            System.out.println("Duplicate contact");
        }
    }
}

