package org.example.mycontact.menu;

import lombok.AllArgsConstructor;
import org.example.mycontact.service.ContactService;
import org.example.mycontact.view.ChoiceView;

@AllArgsConstructor
public class GetContactPartOfName implements Menu {
    private  final ContactService fileContactService;
    private  final ChoiceView choiceView;


    @Override
    public String getName() {
        return "Поиск по имени (части имени)";
    }

    @Override
    public void execute() {
        choiceView.show(fileContactService.findByName(choiceView.readName()));
    }
}
