package org.example.mycontact.menu;

import lombok.AllArgsConstructor;
import org.example.mycontact.service.ContactService;
import org.example.mycontact.view.View;
import org.example.mycontact.view.ViewUser;

@AllArgsConstructor
public class DeleteUserDataBase implements Menu{
    private final ViewUser viewUser;
    private final ContactService contactService;
    @Override
    public String getName() {
        return "Удалить пользователя";
    }

    @Override
    public void execute() {
        String value = viewUser.deleteUser();
        contactService.deletedByValue(value);
    }
}
