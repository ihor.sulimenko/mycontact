package org.example.mycontact.menu;

import lombok.AllArgsConstructor;
import org.example.mycontact.service.ContactService;
import org.example.mycontact.view.ChoiceView;

@AllArgsConstructor
public class DeletedContact implements Menu {

    private final ChoiceView view;
    private final ContactService<String> fileContactService;

    @Override
    public String getName() {
        return "Удаление контакта по значению";
    }

    @Override
    public void execute() {
      String value = view.readContact();
      fileContactService.deletedByValue(value);

    }
}
