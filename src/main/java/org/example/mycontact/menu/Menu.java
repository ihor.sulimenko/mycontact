package org.example.mycontact.menu;

public interface Menu {
    String getName();
    void execute();
    default boolean ifFinal(){
        return false;
    }
}
