package org.example.mycontact.menu;

import lombok.AllArgsConstructor;
import org.example.mycontact.person.Contact;
import org.example.mycontact.service.ContactService;
import org.example.mycontact.view.View;

@AllArgsConstructor
public class SortForSurname implements Menu {

    private final ContactService fileContactService;
    private final View<Contact> view;

    @Override
    public String getName() {
        return "Просмотреть контакты с сортировкой по фамилии";
    }

    @Override
    public void execute() {
        view.show(fileContactService.findAll());
    }
}

