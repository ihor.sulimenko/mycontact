package org.example.mycontact.menu;

import lombok.AllArgsConstructor;
import org.example.mycontact.dto.User;
import org.example.mycontact.service.ContactService;
import org.example.mycontact.view.ViewUser;

@AllArgsConstructor
public class AddUserDataBase implements Menu{
    private final ContactService contactService;
    private final ViewUser viewUser;
    @Override
    public String getName() {
        return "Добавить пользователя";
    }

    @Override
    public void execute() {
        User user = viewUser.readContact();
        contactService.save(user);
    }
}
