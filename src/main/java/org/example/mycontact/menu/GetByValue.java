package org.example.mycontact.menu;

import lombok.AllArgsConstructor;
import org.example.mycontact.service.ContactService;
import org.example.mycontact.view.ChoiceView;
@AllArgsConstructor
public class GetByValue implements Menu {

    private  final ContactService fileContactService;
    private  final ChoiceView view;


    @Override
    public String getName() {
        return "Поиск по началу контакта";
    }

    @Override
    public void execute() {
            String value = view.readContact();
            view.show(fileContactService.findByValueStart(value));
    }
}
