package org.example.mycontact.menu;


import java.util.Scanner;

public class MenuContact {
    private final Scanner scanner;
    private final Menu[] contacts;


    public MenuContact(Scanner scanner, Menu[] contacts) {
        this.scanner = scanner;
        this.contacts = contacts;
    }

    public void run(){
        while (true){
            showMenu();
            int choice  = getUserChoice();
            if(choice < 0 || choice >= contacts.length){
                System.out.println("Неверный ввод");
                continue;
            }
            contacts[choice].execute();
            if (contacts[choice].ifFinal()) break;
        }
    }

    private int getUserChoice() {
        System.out.println("Выберете пункт меню:");
        int ch = scanner.nextInt();
        scanner.nextLine();
        return ch-1;
    }

    public void showMenu() {
        System.out.println("-".repeat(50));
        for (int i = 0; i < contacts.length; i++) {
            System.out.printf("%2d - %s\n", i+1, contacts[i].getName());
       System.out.println("-".repeat(50));}}

}
