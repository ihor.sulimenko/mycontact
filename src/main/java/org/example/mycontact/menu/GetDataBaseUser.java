package org.example.mycontact.menu;

import lombok.AllArgsConstructor;
import org.example.mycontact.service.ContactService;
import org.example.mycontact.view.ViewUser;

@AllArgsConstructor
public class GetDataBaseUser implements Menu {
    private ContactService contactService;
    private ViewUser viewUser;
    @Override
    public String getName() {
        return "Получить список пользователей";
    }

    @Override
    public void execute() {
        viewUser.show(contactService.findAll());
    }
}
