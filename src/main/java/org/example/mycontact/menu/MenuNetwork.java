package org.example.mycontact.menu;

import org.example.mycontact.service.AuthService;

import java.util.Scanner;

public class MenuNetwork {
    private final Scanner scanner;
    private final Menu[] contacts;
    private final AuthService authService;
    private final MenuContact menuContact;

    public MenuNetwork(Scanner scanner, Menu[] contacts, AuthService authService, MenuContact menuContact) {
        this.scanner = scanner;
        this.contacts = contacts;
        this.authService = authService;
        this.menuContact = menuContact;
    }

    public void run() {
        while (true) {
            showMenu();
            int choice = getUserChoice();
            if (choice < 0 || choice >= contacts.length) {
                System.out.println("Неверный ввод");
                continue;
            }
            contacts[choice].execute();
            if (contacts[choice].ifFinal()) break;
        }
    }

    private int getUserChoice() {
        System.out.println("Выберете пункт меню:");
        int ch = scanner.nextInt();
        scanner.nextLine();
        return ch - 1;
    }

    private void showMenu() {
        System.out.println("-".repeat(50));
        if(!authService.isAuth()){
        for (int i = 0; i < 3; i++) {
            System.out.printf("%2d - %s\n", i + 1, contacts[i].getName());
            System.out.println("-".repeat(50));
        }}
        if(authService.isAuth()){
            menuContact.run();
        }
    }

}
