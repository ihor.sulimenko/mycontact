package org.example.mycontact.menu;

import lombok.AllArgsConstructor;
import org.example.mycontact.service.UsersService;

@AllArgsConstructor
public class GetUsers implements Menu {
    UsersService usersService;

    @Override
    public String getName() {
        return "Показать пользователей";
    }

    @Override
    public void execute() {
        System.out.println(usersService.getUsers());
    }
}
