package org.example.mycontact.menu;

import lombok.AllArgsConstructor;
import org.example.mycontact.view.NetworkView;
@AllArgsConstructor
public class EntryUser implements Menu{
    NetworkView view;
    @Override
    public String getName() {
        return "Вход пользователя";
    }

    @Override
    public void execute() {
    view.requestLogin();
    }
}
