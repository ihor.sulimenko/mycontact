package org.example.mycontact.menu;

import lombok.AllArgsConstructor;
import org.example.mycontact.view.NetworkView;

@AllArgsConstructor
public class RegisterNewUser implements Menu {
        NetworkView view;

    @Override
    public String getName() {
        return "Регистрация нового пользователя";
    }

    @Override
    public void execute() {
        view.requestRegister();

    }
}
