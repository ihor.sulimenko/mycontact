package org.example.mycontact.menu;

import lombok.AllArgsConstructor;
import org.example.mycontact.person.Contact;
import org.example.mycontact.person.ContactType;
import org.example.mycontact.service.ContactService;
import org.example.mycontact.view.View;
@AllArgsConstructor
public class GetOnlyNumbers implements Menu{
    private  final ContactService fileContactService;
    private  final View<Contact> view;


    @Override
    public String getName() {
        return "Посмотреть только телефоны";
    }

    @Override
    public void execute() {

        view.show(fileContactService.findPhone(ContactType.PHONE));
    }
}
