package org.example.mycontact.utillits.contact;

import org.example.mycontact.person.Contact;
import org.example.mycontact.person.ContactType;
import org.example.mycontact.utillits.Validator;


public class EmailValidator implements Validator<Contact> {

    @Override
    public boolean isValid(Contact contact) {
        if(contact.getContactType()!= ContactType.EMAIL){return true;}
        return contact.getValue().matches("\\w{4,}@\\w{2,}\\.\\w{2,4}");
    }
}
