package org.example.mycontact.utillits.contact;

import lombok.AllArgsConstructor;
import org.example.mycontact.person.Contact;
import org.example.mycontact.utillits.Validator;

import java.util.List;
@AllArgsConstructor
public class ContactValidator implements Validator<Contact> {
    private  final List<Validator<Contact>> validators;

    @Override
    public boolean isValid(Contact contact) {
        return validators.stream().allMatch(v->v.isValid(contact));
    }
}
