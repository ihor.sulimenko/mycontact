package org.example.mycontact.utillits.contact;

import org.example.mycontact.person.Contact;
import org.example.mycontact.person.ContactType;
import org.example.mycontact.utillits.Validator;

import java.util.regex.Pattern;

public class PhoneValidator implements Validator<Contact> {
    private static final Pattern PHONE_PATTER = Pattern.compile("(\\+380|80|0)\\d{9}");
    @Override
    public boolean isValid(Contact contact) {
        if(contact.getContactType()!= ContactType.PHONE){return true;}
        return PHONE_PATTER.matcher(contact.getValue()).matches();
    }
}
