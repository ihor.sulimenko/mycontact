package org.example.mycontact.utillits;

import lombok.RequiredArgsConstructor;
import org.example.mycontact.Main;

import java.io.FileReader;
import java.io.IOException;
import java.util.Optional;
import java.util.Properties;
@RequiredArgsConstructor

public class MyProperties {
   public   Properties getProperties(String applicationModeKey) {
        String propertiesSource = System.getProperty("property.source");
        propertiesSource = Optional.ofNullable(propertiesSource).orElse("CLASSPATH");
        Properties properties = new Properties();
        try {
            if (propertiesSource.equals("CLASSPATH")) {
                properties.load(
                        Main.class
                                .getClassLoader()
                                .getResourceAsStream("config.properties"));
            } else {
                properties.load(
                        new FileReader(propertiesSource));
            }
        } catch (IOException e) {
            e.printStackTrace();
            properties.setProperty(applicationModeKey, "IN_MEMORY");
        }
        return properties;
    }
}
