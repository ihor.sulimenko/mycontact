package org.example.mycontact.utillits;

import java.sql.ResultSet;
import java.sql.SQLException;

public class IdRowMapper implements RowMapper<Integer> {
    @Override
    public Integer map(ResultSet rs) throws SQLException {
        return rs.getInt("id");
    }
}

