package org.example.mycontact.utillits;

import lombok.AllArgsConstructor;
import lombok.Data;
import org.example.mycontact.factory.*;

import java.net.http.HttpClient;
import java.util.Map;
import java.util.Properties;

@AllArgsConstructor
@Data
public class MyContactRepositoryFactory {
    private MyProperties myProperties;
    private HttpClient httpClient;
    private Validator validator;

    public ContactRepositoryFactory getContactRepositoryFactory(String applicationModeKe) {
        Properties properties = myProperties.getProperties(applicationModeKe);

        String mode = properties.getProperty(applicationModeKe);

        Map<String, ContactRepositoryFactory> factoryMap = Map.of(
                "IN_MEMORY", new InMemoryContactRepositoryFactory(validator),
                "JSON_FILE", new JsonFileContactRepositoryFactory(properties,validator),
                "NETWORK_FILE", new NetworkContactRepositoryFactory(httpClient),
                "DATA_BASE",new DataBaseContactRepositoryFactory(),
                "USERS_DATA_BASE",new ApiDataBase());

        if (!factoryMap.containsKey(mode)) {
            throw new RuntimeException("INVALID MODE CONFIGURATION");
        }
        return factoryMap.get(mode);
    }
}
