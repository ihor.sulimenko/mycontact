package org.example.mycontact.utillits;

import org.example.mycontact.person.ContactForBD;

import java.sql.ResultSet;
import java.sql.SQLException;

public class ContactRowMapper implements RowMapper<ContactForBD> {

    @Override
    public ContactForBD map(ResultSet rs) throws SQLException {
        return new ContactForBD()
                .setId(rs.getInt("id"))
                .setName(rs.getString("name"))
                .setSurname(rs.getString("surname"))
                .setContact_type(rs.getString("contact_type"))
                .setValue(rs.getString("value"));
    }
}

