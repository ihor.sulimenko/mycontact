package org.example.mycontact.utillits;

import lombok.RequiredArgsConstructor;
import org.example.mycontact.dto.User;

import javax.sql.DataSource;
import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;

@RequiredArgsConstructor
public class SqlUsersNetwork {
    private final DataSource dataSource;

    public List<User> getUser() {

        try (Connection connection = dataSource.getConnection()) {
            Statement stmt = connection.createStatement();
            List<User> users = new ArrayList<>();
            ResultSet resultSet = stmt.executeQuery("SELECT (login,password,date_born) from users");
            while (resultSet.next()) {
                users.add(new User(resultSet.getString("login"),
                        resultSet.getString("password"),
                        resultSet.getString("date_born"))
                );
            }
            return users;
        } catch (SQLException e) {
            return List.of();
        }
    }
}
