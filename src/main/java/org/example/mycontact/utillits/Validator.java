package org.example.mycontact.utillits;

public interface Validator<T> {
    boolean isValid(T contact);
}
