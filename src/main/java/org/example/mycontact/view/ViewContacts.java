package org.example.mycontact.view;

import lombok.RequiredArgsConstructor;
import org.example.mycontact.exception.InvalidInputException;
import org.example.mycontact.person.Contact;
import org.example.mycontact.person.ContactType;

import java.util.Scanner;
import java.util.Set;

@RequiredArgsConstructor
public class ViewContacts implements View<Contact> {
    private final Scanner scanner;

    @Override
    public void show(Set<Contact> set) {
        System.out.println(set);
    }

    @Override
    public Contact readContact() {
        System.out.println("Введите Ф.И.О.:");
        System.out.println("Введите Имя");
        String name = scanner.next();
        scanner.nextLine();
        System.out.println("Введите Фамилию");
        String surname = scanner.next();
        scanner.nextLine();
        ContactType contactType = readContactType();
        System.out.println("Выберете тип контакта:");
        String value = scanner.next();
        return new Contact()
                .setName(name)
                .setSurname(surname)
                .setContactType(contactType)
                .setValue(value);
    }

    private ContactType readContactType() {
        ContactType[] types = ContactType.values();
        for (int i = 0; i < types.length; i++) {
            System.out.printf("%d - %s\n", i + 1, types[i].getTypeOfContacts());
        }
        int choice = getChoice();
        if (choice < 0 || choice >= types.length) {
            throw new InvalidInputException();
        }
        return types[choice];
    }

    private int getChoice() {
        System.out.println("Выберете ");
        int choice = scanner.nextInt() - 1;
        scanner.nextLine();
        return choice;
    }
}
