package org.example.mycontact.view;

import org.example.mycontact.person.Contact;

import java.util.Set;

public interface View<T> {
    T readContact();
    void show(Set<Contact> set);
}
