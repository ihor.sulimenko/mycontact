package org.example.mycontact.view;

import lombok.AllArgsConstructor;
import org.example.mycontact.person.Contact;

import java.util.Scanner;
import java.util.Set;

@AllArgsConstructor
public class ChoiceView implements View<String> {
    private final Scanner scanner;

    @Override
    public String readContact() {
        System.out.println("Контакт по email - нажмите 1;Контакт по номеру телефона - нажмите 2 :");
        int choose = scanner.nextInt();
        scanner.nextLine();
        if (choose == 1) {
            System.out.println("Введите email:");
            return scanner.next();

        }
        if (choose == 2) {
            System.out.println("Введите номер телефона:");
            return scanner.next();
        }
        return null;
    }

    @Override
    public void show(Set<Contact> set) {
        System.out.println(set);
    }

    public String readName() {
        System.out.println("Введите имя");
        return scanner.next();
    }
}
