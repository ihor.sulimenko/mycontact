package org.example.mycontact.view;

import lombok.AllArgsConstructor;
import org.example.mycontact.service.AuthService;

import java.util.Scanner;
@AllArgsConstructor
public class ViewStatusNewUser implements NetworkView{
    Scanner scanner;
    AuthService authService;

    @Override
    public void requestRegister() {
        System.out.println("Введите данные для регистрации");
        System.out.println("-".repeat(20));
        System.out.println("Введите логин");
        String login = scanner.next();
        scanner.nextLine();
        System.out.println("Введите пароль");
        String password = scanner.next();
        scanner.nextLine();
        System.out.println("Введите дату рождения");
        String dateBorn = scanner.next();
        scanner.nextLine();
        authService.register(login,password,dateBorn);
    }

    @Override
    public void requestLogin() {
        System.out.println("Введите данные для входа");
        System.out.println("-".repeat(20));
        System.out.println("Введите логин");
        String login = scanner.next();
        scanner.nextLine();
        System.out.println("Введите пароль");
        String password = scanner.next();
        scanner.nextLine();
        authService.login(login,password);
    }
}
