package org.example.mycontact.view;

public interface NetworkView {
    void requestRegister();
    void requestLogin();
}
