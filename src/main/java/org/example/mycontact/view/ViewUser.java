package org.example.mycontact.view;

import lombok.RequiredArgsConstructor;
import org.example.mycontact.dto.User;

import java.util.Scanner;
import java.util.Set;

@RequiredArgsConstructor
public class ViewUser implements View<User> {
    private final Scanner scanner;

    public String deleteUser(){
        System.out.println("Ведите login");
        return scanner.next();
    }

    @Override
    public User readContact() {
        System.out.println("Введите login");
        String login = scanner.next();
        scanner.nextLine();
        System.out.println("Введите password");
        String password = scanner.next();
        scanner.nextLine();
        System.out.println("Выберете date_born");
        String date_born = scanner.next();

        return new User(login,password,date_born) ;
    }

    @Override
    public void show(Set set) {
        System.out.println(set);
    }
}
