package org.example.mycontact.exception;

public class ValidateExeption extends RuntimeException {
    public ValidateExeption(String message) {
        super(message);
    }
}
