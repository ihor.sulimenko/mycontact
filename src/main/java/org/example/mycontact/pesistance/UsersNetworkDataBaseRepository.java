package org.example.mycontact.pesistance;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import lombok.AllArgsConstructor;
import org.example.mycontact.dto.User;
import org.example.mycontact.exception.OperationNotSupported;
import org.example.mycontact.person.ContactType;

import java.io.*;
import java.net.URI;
import java.net.http.HttpClient;
import java.net.http.HttpRequest;
import java.net.http.HttpResponse;
import java.util.*;

@AllArgsConstructor
public class UsersNetworkDataBaseRepository implements ContactRepository<User> {

    private final HttpClient httpClient;


    @Override
    public void save(User contact) {
        ObjectMapper objectMapper = new ObjectMapper();
        String sendUser = "";
        try {
            sendUser = objectMapper.writeValueAsString(contact);
        } catch (JsonProcessingException e) {
            e.printStackTrace();
        }
        try {
            HttpRequest httpRequest = HttpRequest.newBuilder()
                    .POST(HttpRequest.BodyPublishers.ofString(sendUser))
                    .header("Content-Type", "application/json")
                    .uri(URI.create("http://localhost:8080/MyContact_war_exploded/add"))
                    .build();
            HttpResponse<String> response = httpClient.send(httpRequest, HttpResponse.BodyHandlers.ofString());
            if (response.statusCode() == 200) {
                System.out.println("Пользователь добавлен успешно");
            } else {
                throw new InterruptedException();
            }
        } catch (IOException | InterruptedException e) {
            e.printStackTrace();
        }
    }

    @Override
    public Set sortBySurname() {
        Set<String> userSet = new HashSet<>();
        try {
            HttpRequest httpRequest = HttpRequest.newBuilder()
                    .GET()
                    .header("Content-Type", "application/json")
                    .uri(URI.create("http://localhost:8080/MyContact_war_exploded/show"))
                    .build();
            HttpResponse<String> response = httpClient.send(httpRequest, HttpResponse.BodyHandlers.ofString());
            userSet.add(response.body());
            if (response.statusCode() != 200) {
                throw new RuntimeException();
            }
        } catch (IOException | InterruptedException e) {
            e.printStackTrace();
        }
        return userSet;
    }

    @Override
    public Set<User> findByType(ContactType type) {
        throw new OperationNotSupported();
    }

    @Override
    public Set<User> findByValueStart(String valueStart) {
        throw new OperationNotSupported();
    }

    @Override
    public Set<User> findByName(String namePart) {
        throw new OperationNotSupported();
    }

    @Override
    public Optional<User> findByValue(String value) {
        throw new OperationNotSupported();
    }

    @Override
    public void deletedByValue(String value) {
        try {
            HttpRequest httpRequest = HttpRequest.newBuilder()
                    .POST(HttpRequest.BodyPublishers.ofString(value))
                    .header("Content-Type", "application/json")
                    .uri(URI.create("http://localhost:8080/MyContact_war_exploded/delete"))
                    .build();
            HttpResponse<String> response = httpClient.send(httpRequest, HttpResponse.BodyHandlers.ofString());
            System.out.println(response.statusCode());
            if (response.statusCode() != 200) {
                throw new RuntimeException();
            }
        } catch (IOException | InterruptedException e) {
            e.printStackTrace();
        }
    }
}

