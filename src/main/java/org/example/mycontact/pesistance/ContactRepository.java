package org.example.mycontact.pesistance;

import org.example.mycontact.person.Contact;
import org.example.mycontact.person.ContactType;

import java.util.Optional;
import java.util.Set;

public interface ContactRepository<T> {
    void save(T contact);
    Set<T> sortBySurname();
    Set<T> findByType(ContactType type);
    Set<T> findByValueStart(String valueStart);
    Set<T> findByName(String namePart);
    Optional<T> findByValue(String value);
    void deletedByValue(String  value);

}
