package org.example.mycontact.pesistance;

import org.example.mycontact.person.Contact;
import org.example.mycontact.person.ContactType;

import java.util.*;
import java.util.stream.Collectors;

public abstract class AbstractFileContactsRepository  implements ContactRepository<Contact> {

    protected abstract Set<Contact> readAll();

    protected abstract void saveAll(Set<Contact> contacts);

    @Override
    public void save(Contact contact) {
        Set<Contact> contacts = readAll();
        contact.setId(UUID.randomUUID().toString());
        contacts.add(contact);
        saveAll(contacts);
    }

    @Override
    public Set<Contact> sortBySurname() {
        return readAll().stream()
                .sorted(Contact.bySurnameComparator())
                .collect(Collectors.toCollection(LinkedHashSet::new));
    }

    @Override
    public Set<Contact> findByType(ContactType type) {
        return readAll().stream()
                .filter(c -> c.getContactType() == type)
                .collect(Collectors.toSet());
    }

    @Override
    public Set<Contact> findByValueStart(String valueStart) {
        return readAll().stream()
                .filter(c -> Objects.nonNull(c.getValue()))
                .filter(c -> c.getValue().startsWith(valueStart))
                .collect(Collectors.toSet());
    }

    @Override
    public Set<Contact> findByName(String namePart) {
        return readAll().stream()
                .filter(c -> Objects.nonNull(c.fullName()))
                .filter(c -> c.fullName().contains(namePart))
                .collect(Collectors.toSet());
    }

    @Override
    public Optional<Contact> findByValue(String value) {
        return readAll().stream()
                .filter(c -> Objects.nonNull(c.getValue()))
                .filter(c -> c.getValue().equals(value))
                .findFirst();
    }

    @Override
    public void deletedByValue(String value) {
        Set<Contact> toSave = readAll().stream()
                .filter(c -> !c.getValue().equals(value))
                .collect(Collectors.toSet());
        saveAll(toSave);
    }
}
