package org.example.mycontact.pesistance;

import lombok.AllArgsConstructor;
import org.example.mycontact.person.Contact;
import org.example.mycontact.person.ContactType;

import java.util.*;
import java.util.stream.Collectors;
@AllArgsConstructor
public class InMemoryRepository implements ContactRepository<Contact>{
    Set<Contact> contactSet = new HashSet<>();

    @Override
    public void save(Contact contact) {
        contact.setId(UUID.randomUUID().toString());
        contactSet.add(contact);
    }

    @Override
    public Set<Contact> sortBySurname() {
        return contactSet.stream()
                .sorted(Contact.bySurnameComparator())
                .collect(Collectors.toCollection(LinkedHashSet::new));
    }

    @Override
    public Set<Contact> findByType(ContactType type) {
        return contactSet.stream()
                .filter(c -> c.getContactType() == type)
                .sorted(Contact.bySurnameComparator())
                .collect(Collectors.toCollection(LinkedHashSet::new));
    }

    @Override
    public Set<Contact> findByValueStart(String valueStart) {
        return contactSet.stream()
                .filter(e -> e.getValue().startsWith(valueStart))
                .sorted(Contact.bySurnameComparator())
                .collect(Collectors.toCollection(LinkedHashSet::new));
    }

    @Override
    public Set<Contact> findByName(String namePart) {
        return contactSet.stream().filter(e -> e.fullName()
                        .contains(namePart))
                .collect(Collectors.toSet());
    }

    @Override
    public Optional<Contact> findByValue(String value) {
        return contactSet.stream().filter(e -> e.fullName()
                        .equals(value))
                .findFirst();
    }

    @Override
    public void deletedByValue(String value) {
        contactSet = contactSet.stream()
                .filter(c->!c.getValue().equals(value))
                .collect(Collectors.toSet());
    }
}
