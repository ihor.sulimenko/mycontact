package org.example.mycontact.pesistance;

import lombok.RequiredArgsConstructor;
import org.example.mycontact.person.Contact;
import org.example.mycontact.person.ContactForBD;
import org.example.mycontact.person.ContactType;
import org.example.mycontact.service.JdbcTemplate;
import org.example.mycontact.service.NetworkDataBaseAuthService;
import org.example.mycontact.utillits.ContactRowMapper;

import java.util.HashSet;
import java.util.List;
import java.util.Optional;
import java.util.Set;

@RequiredArgsConstructor
public class DbPostgresRepository implements ContactRepository<Contact> {
    private final JdbcTemplate jdbcTemplate;
    private final Set<List<ContactForBD>> setContact = new HashSet();
    private final NetworkDataBaseAuthService networkDataBaseAuthService;
    private int id;
    @Override
    public void save(Contact contact) {
        utilsForMethod();
        jdbcTemplate.update("INSERT INTO contacts (name,surname,contact_type,value,users_id) VALUES(?,?,?,?,?) ",
                new Object[]{contact.getName(), contact.getSurname(), contact.getContactType().getTypeOfContacts(),
                        contact.getValue(),id});
    }

    @Override
    public Set sortBySurname() {
        utilsForMethod();
        setContact.add(jdbcTemplate.queryParams("SELECT id,name,surname,contact_type,value,users_id " +
                        "FROM contacts WHERE users_id=? ORDER BY surname",
                new Object[]{id},
                new ContactRowMapper()));
        return setContact;
    }

    @Override
    public Set findByType(ContactType type) {
        utilsForMethod();
        setContact.add(jdbcTemplate.queryParams("SELECT id,name,surname,contact_type,value,users_id " +
                        "FROM contacts WHERE contact_type LIKE ? AND users_id=? GROUP BY id ",
                new Object[]{type,id},
                new ContactRowMapper())
        );
        return setContact;
    }

    @Override
    public Set findByValueStart(String valueStart) {
        utilsForMethod();
        setContact.add(jdbcTemplate.queryParams("SELECT id,name,surname,contact_type,value,users_id " +
                        "FROM contacts WHERE value LIKE ? AND users_id=?  ORDER BY id",
                new Object[]{"%" + valueStart + "%",id},
                new ContactRowMapper())
        );
        return setContact;
    }

    @Override
    public Set findByName(String namePart) {
        utilsForMethod();
        setContact.add(jdbcTemplate.queryParams("SELECT *" +
                        "FROM contacts WHERE name LIKE ? AND users_id=? ORDER BY id ",
                new Object[]{"%" + namePart + "%",id},
                new ContactRowMapper())
        );
        return setContact;
    }

    @Override
    public Optional findByValue(String value) {
        utilsForMethod();
        setContact.add(jdbcTemplate.queryParams("SELECT id,name,surname,contact_type,value,users_id " +
                        "FROM contacts " +
                        "WHERE value LIKE ? AND users_id=? ORDER BY id",
                new Object[]{value,id},
                new ContactRowMapper())
        );
        return Optional.of(setContact);
    }

    @Override
    public void deletedByValue(String value) {
        utilsForMethod();
        jdbcTemplate.update("DELETE FROM contacts WHERE value LIKE ? AND users_id=?",
                new Object[]{value,id});
    }
    public void utilsForMethod(){
        id = networkDataBaseAuthService.getIdUser();
        setContact.clear();
    }
}
