package org.example.mycontact.pesistance;

import lombok.RequiredArgsConstructor;
import org.example.mycontact.person.Contact;

import java.io.*;
import java.util.HashSet;
import java.util.Set;

@RequiredArgsConstructor
public class ByteFileContactRepository extends AbstractFileContactsRepository {
    private final InMemoryRepository inMemoryRepository;


    @Override
    protected Set<Contact> readAll() {
        Set<Contact> contacts = null;
        try (ObjectInputStream inputStream = new ObjectInputStream(new FileInputStream("contactsBite.dat"))) {
            int size = inputStream.readInt();
            contacts = new HashSet<>(size);
            for (int i = 0; i < size; i++) {
                contacts.add((Contact) inputStream.readObject());
            }

        } catch (IOException | ClassNotFoundException e) {
            e.printStackTrace();
        }
        return contacts;
    }

    @Override
    protected void saveAll(Set<Contact> contacts) {
        Set<Contact> contactsFile = inMemoryRepository.contactSet;
        try (ObjectOutputStream os = new ObjectOutputStream(new FileOutputStream("contactsBite.dat"))) {
            os.writeInt(contactsFile.size());
            for (Contact contact : contactsFile) {
                os.writeObject(contact);
            }
            os.flush();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
}
