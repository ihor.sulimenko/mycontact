package org.example.mycontact.pesistance;

import com.google.gson.Gson;
import lombok.AllArgsConstructor;
import lombok.RequiredArgsConstructor;
import org.example.mycontact.exception.AuthorizationException;
import org.example.mycontact.exception.InvalidInputException;
import org.example.mycontact.person.Contact;
import org.example.mycontact.person.ContactType;
import org.example.mycontact.person.NetworkContact;
import org.example.mycontact.service.NetworkAuthService;

import java.io.IOException;
import java.net.URI;
import java.net.http.HttpClient;
import java.net.http.HttpRequest;
import java.net.http.HttpResponse;
import java.util.HashSet;
import java.util.Optional;
import java.util.Set;

@AllArgsConstructor
@RequiredArgsConstructor
public class NetworkFileRepository implements ContactRepository<Contact> {

    private final HttpClient httpClient;
    private  NetworkAuthService authService;

    @Override
    public void save(Contact contact) {
        Gson gson = new Gson();
        NetworkContact networkContact = new NetworkContact(contact.getContactType().getTypeOfContacts(),
                contact.getValue(),contact.getName());
        String newUser = gson.toJson(networkContact);

        try {
            HttpRequest httpRequest = HttpRequest.newBuilder()
                    .POST(HttpRequest.BodyPublishers.ofString(newUser))
                    .header("Authorization", "Bearer " + authService.getToken())
                    .header("Accept", "application/json")
                    .header("Content-Type", "application/json")
                    .uri(URI.create("https://mag-contacts-api.herokuapp.com/contacts/add"))
                    .build();
            HttpResponse<String> response = httpClient.send(httpRequest, HttpResponse.BodyHandlers.ofString());
            if (response.statusCode() != 200) {
                throw new AuthorizationException();
            }
        } catch (IOException | InterruptedException e) {
            e.printStackTrace();
        }
    }

    @Override
    public Set sortBySurname() {
        Set<String> sort = new HashSet<>();
        try {
            HttpRequest httpRequest = HttpRequest.newBuilder()
                    .GET()
                    .header("Authorization", "Bearer " + authService.getToken())
                    .header("Accept",
                            "application/json")
                    .header("Content-Type", "application/json")
                    .uri(URI.create("https://mag-contacts-api.herokuapp.com/contacts"))
                    .build();
            HttpResponse<String> response = httpClient.send(httpRequest, HttpResponse.BodyHandlers.ofString());
            sort.add(response.body());
            if (response.statusCode() != 200) {
                throw new AuthorizationException();
            }
        } catch (IOException | InterruptedException e) {
            e.printStackTrace();
        }
        return sort;
    }

    @Override
    public Set findByType(ContactType type) {
        throw new InvalidInputException();
    }

    @Override
    public Set findByValueStart(String valueStart) {
        Gson gson = new Gson();
        Contact contact = new Contact();
        contact.setValue(valueStart);
        String value = gson.toJson(contact);
        Set<String> sort = new HashSet<>();
        try {
            HttpRequest httpRequest = HttpRequest.newBuilder()
                    .POST(HttpRequest.BodyPublishers.ofString(value))
                    .header("Authorization", "Bearer " + authService.getToken())
                    .header("Accept",
                            "application/json")
                    .header("Content-Type", "application/json")
                    .uri(URI.create("https://mag-contacts-api.herokuapp.com/contacts/find"))
                    .build();
            HttpResponse<String> response = httpClient.send(httpRequest, HttpResponse.BodyHandlers.ofString());
            if (response.statusCode() != 200) {
                throw new AuthorizationException();
            }
            sort.add(response.body());
        } catch (IOException | InterruptedException e) {
            e.printStackTrace();
        }
        return sort;
    }

    @Override
    public Set findByName(String namePart) {
        Gson gson = new Gson();
        Contact contact = new Contact();
        contact.setName(namePart);
        String name = gson.toJson(contact);
        Set<String> sort = new HashSet<>();
        try {
            HttpRequest httpRequest = HttpRequest.newBuilder()
                    .POST(HttpRequest.BodyPublishers.ofString(name))
                    .header("Authorization", "Bearer " + authService.getToken())
                    .header("Accept",
                            "application/json")
                    .header("Content-Type", "application/json")
                    .uri(URI.create("https://mag-contacts-api.herokuapp.com/contacts/find"))
                    .build();
            HttpResponse<String> response = httpClient.send(httpRequest, HttpResponse.BodyHandlers.ofString());
            if (response.statusCode() != 200) {
                throw new AuthorizationException();
            }
            sort.add(response.body());
        } catch (IOException | InterruptedException e) {
            e.printStackTrace();
        }
        return sort;
    }

    @Override
    public Optional findByValue(String value) {
        throw new InvalidInputException();
    }

    @Override
    public void deletedByValue(String value) {
        throw new InvalidInputException();
    }
}

