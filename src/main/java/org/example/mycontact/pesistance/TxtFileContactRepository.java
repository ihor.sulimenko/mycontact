package org.example.mycontact.pesistance;

import lombok.RequiredArgsConstructor;
import org.example.mycontact.person.Contact;

import java.io.*;
import java.nio.charset.StandardCharsets;
import java.util.HashSet;
import java.util.Set;

@RequiredArgsConstructor
public class TxtFileContactRepository extends AbstractFileContactsRepository {

    private final InMemoryRepository inMemoryRepository;

    @Override
    protected Set<Contact> readAll() {
        Set<Contact> txtFile = new HashSet<>();
        try (BufferedReader bufferedReader = new BufferedReader(new FileReader("contacts.txt"))) {
            while ((true)) {
                String line = bufferedReader.readLine();
                if (line == null) {
                    return txtFile;
                }
                txtFile.add(new Contact().setName(line));
            }
        } catch (IOException e) {
            e.printStackTrace();
        }
        return txtFile;
    }


    @Override
    protected void saveAll(Set<Contact> contacts) {
        Set<Contact> contactsFile = inMemoryRepository.contactSet;
        try (OutputStream os = new BufferedOutputStream(new FileOutputStream("contacts.txt"))) {
            for (Contact contactTxt : contactsFile) {
                String listContacts = contactTxt+ "\n";
                os.write(listContacts.getBytes(StandardCharsets.UTF_8));
            }
            os.flush();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
}
