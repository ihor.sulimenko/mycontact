package org.example.mycontact.pesistance;

import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.ObjectMapper;
import lombok.RequiredArgsConstructor;
import org.example.mycontact.person.Contact;

import java.io.*;
import java.util.HashSet;
import java.util.Set;
@RequiredArgsConstructor
public class JsonFileContactRepository  extends AbstractFileContactsRepository{

    private final ObjectMapper objectMapper;
    private final String fileName;

    @Override
    public Set<Contact> readAll() {
       try(InputStream is = new FileInputStream(fileName)){
        return objectMapper.readValue(is, new TypeReference<HashSet<Contact>>() {
        });
       }
       catch (IOException e) {
           e.printStackTrace();
           return new HashSet<>();
       }

    }

    @Override
    protected void saveAll(Set<Contact> contacts) {
        try(OutputStream os = new FileOutputStream(fileName)){
            byte[] jsonByte = objectMapper.writeValueAsBytes(contacts);
            os.write(jsonByte);
            os.flush();
        }
        catch (IOException e) {
            e.printStackTrace();
        }

    }
}
