package org.example.mycontact.person;

public class NetworkContact {
    private String type;
    private String value;
    private String name;

    public NetworkContact(String type, String value, String name) {
        this.type = type;
        this.value = value;
        this.name = name;
    }
}
