package org.example.mycontact.person;

import lombok.Data;
import lombok.experimental.Accessors;

import java.io.Serializable;

@Data
@Accessors(chain = true)
public class ContactForBD implements Serializable {
    private Integer id;
    private String name;
    private String surname;
    private String contact_type;
    private String value;

}
