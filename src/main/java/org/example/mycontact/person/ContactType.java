package org.example.mycontact.person;

public enum ContactType {
    EMAIL("email"),
    PHONE("number");



    ContactType(String type) {
        this.typeOfContacts=type;
    }
    private final String typeOfContacts;

    public String getTypeOfContacts() {
        return typeOfContacts;
    }
}
