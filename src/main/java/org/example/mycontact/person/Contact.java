package org.example.mycontact.person;

import com.fasterxml.jackson.annotation.JsonIgnore;
import lombok.Data;
import lombok.experimental.Accessors;

import java.io.Serializable;
import java.util.Comparator;
import java.util.Objects;

@Data
@Accessors(chain = true)
public class Contact implements Serializable {
    private String name;
    private String surname;
    private String id;
    private ContactType contactType;
    private String value;


    @Override
    public String toString() {
        return "Contact{" +
                "name='" + name + '\'' +
                ", surname='" + surname + '\'' +
                ", id='" + id + '\'' +
                ", contactType=" + contactType +
                ", value='" + value + '\'' +
                '}';
    }

    @JsonIgnore
    public String fullName() {
        String fullName= " ";
        return fullName=surname + " " + name;
    }

    public static Comparator<Contact> bySurnameComparator() {
        return (Contact a, Contact b) -> {
            if (a == null && b == null) {
                return 0;
            }
            if(a==null){return -1;}
            if(b==null){return 1;}
            int cmp = Objects.compare(a.surname, b.surname, String::compareTo);
            if (cmp == 0) {
                cmp = Objects.compare(a.name, b.name, String::compareTo);
            }
            return cmp;
        };
    }
}
