package org.example.mycontact.factory;

import lombok.AllArgsConstructor;
import org.example.mycontact.person.Contact;
import org.example.mycontact.pesistance.ContactRepository;
import org.example.mycontact.pesistance.InMemoryRepository;
import org.example.mycontact.service.AuthService;
import org.example.mycontact.service.ContactService;
import org.example.mycontact.service.FileContactService;
import org.example.mycontact.service.InMemoryAuthService;
import org.example.mycontact.utillits.Validator;

import java.util.HashSet;
import java.util.Set;
@AllArgsConstructor
public class InMemoryContactRepositoryFactory implements ContactRepositoryFactory {
    private Validator<Contact> validator;

    @Override
    public ContactRepository<Contact> createContactRepository() {
        return new InMemoryRepository(initialContact());
    }

    @Override
    public AuthService createAuthService() {
        return new InMemoryAuthService();
    }

    @Override
    public ContactService createService() {
        return new FileContactService(createContactRepository(),validator);
    }

    private Set<Contact> initialContact() {
        return new HashSet<>();
    }
}
