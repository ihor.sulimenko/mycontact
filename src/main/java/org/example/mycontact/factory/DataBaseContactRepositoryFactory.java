package org.example.mycontact.factory;

import com.zaxxer.hikari.HikariConfig;
import com.zaxxer.hikari.HikariDataSource;
import lombok.AllArgsConstructor;
import lombok.RequiredArgsConstructor;
import org.example.mycontact.pesistance.ContactRepository;
import org.example.mycontact.pesistance.DbPostgresRepository;
import org.example.mycontact.service.*;

import javax.sql.DataSource;

@AllArgsConstructor
@RequiredArgsConstructor
public class DataBaseContactRepositoryFactory implements ContactRepositoryFactory {

    @Override
    public ContactRepository createContactRepository() {
       NetworkDataBaseAuthService networkDataBaseAuthService = createAuthService();
//       dataSource = new HikariDataSource(networkDataBaseAuthService);
//       return new DbPostgresRepository(new JdbcTemplate(dataSource));
        return null;
    }

    private NetworkDataBaseAuthService networkDataBaseAuthService;
    private DataSource dataSource;

    @Override
    public NetworkDataBaseAuthService createAuthService() {
        if (networkDataBaseAuthService == null) {
//            networkDataBaseAuthService = new NetworkDataBaseAuthService();
        }
        return networkDataBaseAuthService;
    }

    @Override
    public ContactService createService() {
        return new DataBaseService(createContactRepository());
    }
}
