package org.example.mycontact.factory;

import com.fasterxml.jackson.databind.ObjectMapper;
import lombok.AllArgsConstructor;
import lombok.RequiredArgsConstructor;
import org.example.mycontact.person.Contact;
import org.example.mycontact.pesistance.ContactRepository;
import org.example.mycontact.pesistance.JsonFileContactRepository;
import org.example.mycontact.service.AuthService;
import org.example.mycontact.service.ContactService;
import org.example.mycontact.service.FileContactService;
import org.example.mycontact.service.InMemoryAuthService;
import org.example.mycontact.utillits.Validator;

import java.util.Properties;
@AllArgsConstructor
@RequiredArgsConstructor
public class JsonFileContactRepositoryFactory implements ContactRepositoryFactory {
    private final Properties properties;
    private Validator<Contact> validator;
    @Override
    public ContactRepository<Contact> createContactRepository() {
        return new JsonFileContactRepository(createObjectMapper(),properties.getProperty("json.file.name"));
    }

    @Override
    public AuthService createAuthService() {
        return new InMemoryAuthService();
    }

    @Override
    public ContactService createService() {

        return new FileContactService(createContactRepository(),validator);
    }

    private ObjectMapper createObjectMapper()    {
        return new ObjectMapper();
    }
}
