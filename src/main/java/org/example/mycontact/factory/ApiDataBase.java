package org.example.mycontact.factory;

import com.fasterxml.jackson.databind.ObjectMapper;
import org.example.mycontact.pesistance.UsersNetworkDataBaseRepository;
import org.example.mycontact.service.AuthService;
import org.example.mycontact.service.ContactService;
import org.example.mycontact.service.InMemoryAuthService;
import org.example.mycontact.service.LocalDataBase;

import java.net.http.HttpClient;

public class ApiDataBase implements ContactRepositoryFactory {
    @Override
    public UsersNetworkDataBaseRepository createContactRepository() {
        createAuthService();
        return new UsersNetworkDataBaseRepository(httpClient);
    }

    private HttpClient httpClient;

    @Override
    public AuthService createAuthService() {
        if (httpClient == null) {
            httpClient = HttpClient.newBuilder().build();
        }
        return new InMemoryAuthService();
    }

    @Override
    public ContactService createService() {
        return new LocalDataBase(createContactRepository());
    }
}
