package org.example.mycontact.factory;

import lombok.RequiredArgsConstructor;
import org.example.mycontact.pesistance.NetworkFileRepository;
import org.example.mycontact.service.ContactService;
import org.example.mycontact.service.NetworkAuthService;
import org.example.mycontact.service.NetworkContactService;

import java.net.http.HttpClient;

@RequiredArgsConstructor
public class NetworkContactRepositoryFactory implements ContactRepositoryFactory {
    private final HttpClient httpClient;

    @Override
    public NetworkFileRepository createContactRepository() {
        NetworkAuthService authService = createAuthService();
        return new NetworkFileRepository(httpClient,authService);
    }

    private NetworkAuthService networkAuthService;

    @Override
    public NetworkAuthService createAuthService() {
        if (networkAuthService == null) {
            networkAuthService = new NetworkAuthService(httpClient);
        }
        return networkAuthService;
    }

    @Override
    public ContactService createService() {
        return new NetworkContactService(createContactRepository());
    }

}
