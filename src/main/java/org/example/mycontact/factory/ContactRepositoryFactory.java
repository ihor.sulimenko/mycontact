package org.example.mycontact.factory;

import org.example.mycontact.pesistance.ContactRepository;
import org.example.mycontact.service.AuthService;
import org.example.mycontact.service.ContactService;

public interface ContactRepositoryFactory  {
    ContactRepository createContactRepository();
    AuthService createAuthService();
    ContactService createService();

}
