package org.example.mycontact.service;

import org.example.mycontact.person.Contact;
import org.example.mycontact.person.ContactType;

import java.util.Set;

public interface ContactService<T> {
    void save(T contact);

    Set<T> findAll();

    Set<T> findEmail(ContactType type);

    Set<T> findPhone(ContactType type);

    Set<T> findByValueStart(String valueStart);

    Set<T> findByName(String namePart);

    void deletedByValue(String value);

}
