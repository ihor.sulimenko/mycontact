package org.example.mycontact.service;

import lombok.RequiredArgsConstructor;
import org.example.mycontact.person.Contact;
import org.example.mycontact.person.ContactType;
import org.example.mycontact.pesistance.ContactRepository;
import org.example.mycontact.service.ContactService;

import java.util.Set;

@RequiredArgsConstructor
public class DataBaseService implements ContactService<Contact> {
    private final ContactRepository contactRepository;

    @Override
    public void save(Contact contact) {
        contactRepository.save(contact);
    }

    @Override
    public Set findAll() {
        return contactRepository.sortBySurname();
    }

    @Override
    public Set findEmail(ContactType type) {
        return contactRepository.findByType(ContactType.EMAIL);
    }

    @Override
    public Set findPhone(ContactType type) {
        return contactRepository.findByType(ContactType.PHONE);
    }

    @Override
    public Set findByValueStart(String valueStart) {
        return contactRepository.findByValueStart(valueStart);
    }

    @Override
    public Set findByName(String namePart) {
        return contactRepository.findByName(namePart);
    }

    @Override
    public void deletedByValue(String value) {
        contactRepository.deletedByValue(value);
    }
}
