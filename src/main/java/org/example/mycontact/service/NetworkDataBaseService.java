package org.example.mycontact.service;

import lombok.RequiredArgsConstructor;
import org.example.mycontact.person.Contact;
import org.example.mycontact.person.ContactType;
import org.example.mycontact.pesistance.ContactRepository;

import java.util.Set;

@RequiredArgsConstructor
public class NetworkDataBaseService implements ContactService<Contact> {
    private final ContactRepository contactRepository;

    @Override
    public void save(Contact contact) {
        contactRepository.save(contact);
    }

    @Override
    public Set findAll() {
        return contactRepository.sortBySurname();
    }

    @Override
    public Set findEmail(ContactType type) {
        throw new UnsupportedOperationException();
    }

    @Override
    public Set findPhone(ContactType type) {
        throw new UnsupportedOperationException();
    }

    @Override
    public Set findByValueStart(String valueStart) {
        throw new UnsupportedOperationException();
    }

    @Override
    public Set findByName(String namePart) {
        throw new UnsupportedOperationException();
    }

    @Override
    public void deletedByValue(String value) {
        contactRepository.deletedByValue(value);
    }
}
