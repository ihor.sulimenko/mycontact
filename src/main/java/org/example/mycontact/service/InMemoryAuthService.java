package org.example.mycontact.service;

import org.example.mycontact.exception.AuthorizationException;

public class InMemoryAuthService implements AuthService {
    private final static String LOGIN="admin";
    private final static String PASSWORD="admin";
    private boolean auth = false;

    @Override
    public void login(String login, String password) {
        if(!login.equals(LOGIN)|| !password.equals(PASSWORD)){
            throw new AuthorizationException();
        }
        auth=true;
    }

    @Override
    public void register(String login, String dateBorn, String password) {
        throw new UnsupportedOperationException();
    }

    @Override
    public boolean isAuth() {
        return auth;
    }
}
