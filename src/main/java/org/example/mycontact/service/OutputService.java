package org.example.mycontact.service;


import java.util.List;

public interface OutputService<T> {
     void writeFile();
     List<T> readFile();
}
