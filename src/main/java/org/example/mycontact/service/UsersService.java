package org.example.mycontact.service;

import com.fasterxml.jackson.databind.ObjectMapper;
import lombok.AllArgsConstructor;
import lombok.Data;
import org.example.mycontact.exception.AuthorizationException;
import org.example.mycontact.exception.OperationNotSupported;

import java.io.IOException;
import java.net.URI;
import java.net.http.HttpClient;
import java.net.http.HttpRequest;
import java.net.http.HttpResponse;
import java.util.List;

@Data
@AllArgsConstructor
public class UsersService {
    private final HttpClient httpClient;
    private String uri;
    public List<String> getUsers(){
        HttpRequest httpRequest = HttpRequest.newBuilder()
                .GET()
                .uri(URI.create(uri))
                .build();
        try {
            HttpResponse<String> response = httpClient.send(httpRequest,HttpResponse.BodyHandlers.ofString());
            return List.of(response.body());
        } catch (IOException |InterruptedException e) {
            throw new AuthorizationException();
        }
    }
}
