package org.example.mycontact.service;


import lombok.AllArgsConstructor;
import org.example.mycontact.dto.User;
import org.example.mycontact.person.ContactType;
import org.example.mycontact.pesistance.UsersNetworkDataBaseRepository;

import java.util.Set;
@AllArgsConstructor
public class LocalDataBase implements ContactService<User>{
    private UsersNetworkDataBaseRepository userDataBaseRepository;
    @Override
    public void save(User contact) {
        userDataBaseRepository.save(contact);
    }

    @Override
    public Set findAll() {
        return userDataBaseRepository.sortBySurname();

    }

    @Override
    public Set findEmail(ContactType type) {
        return userDataBaseRepository.findByType(ContactType.EMAIL);
    }

    @Override
    public Set findPhone(ContactType type) {
        return userDataBaseRepository.findByType(ContactType.PHONE);
    }

    @Override
    public Set findByValueStart(String valueStart) {
        return userDataBaseRepository.findByValueStart(valueStart);
    }

    @Override
    public Set findByName(String namePart) {
        return userDataBaseRepository.findByName(namePart);
    }

    @Override
    public void deletedByValue(String value) {
        userDataBaseRepository.deletedByValue(value);
    }

}
