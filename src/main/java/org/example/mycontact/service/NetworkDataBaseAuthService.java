package org.example.mycontact.service;

import lombok.Data;
import lombok.RequiredArgsConstructor;
import org.example.mycontact.utillits.IdRowMapper;

import java.util.List;

@Data
@RequiredArgsConstructor
public class NetworkDataBaseAuthService implements AuthService {
    private boolean auth;
    private final JdbcTemplate jdbcTemplate;
    private int idUser;


    @Override
    public void login(String login, String password) {
        List<Integer> list = jdbcTemplate.queryParams("select (id) from users where login like ? ",
                new Object[]{login},
                new IdRowMapper()
        );
        idUser = list.get(0);
        auth = true;
    }

    @Override
    public void register(String login, String dateBorn, String password) {
        jdbcTemplate.update("INSERT INTO users (login,password,date_born) VALUES(?,?,?) ",
                new Object[]{login, password, dateBorn});
    }

    @Override
    public boolean isAuth() {
        return auth;
    }
}
