package org.example.mycontact.service;

import lombok.AllArgsConstructor;
import org.example.mycontact.exception.InvalidInputException;
import org.example.mycontact.exception.OperationNotSupported;
import org.example.mycontact.person.Contact;
import org.example.mycontact.person.ContactType;
import org.example.mycontact.pesistance.NetworkFileRepository;

import java.util.Set;

@AllArgsConstructor
public class NetworkContactService implements ContactService<Contact>{
    private final NetworkFileRepository networkFileRepository;

    public void save(Contact contact) {
        networkFileRepository.save(contact);
    }

    public Set findAll() {
        return networkFileRepository.sortBySurname();
    }

    @Override
    public Set findEmail(ContactType type) {
        throw new OperationNotSupported();
    }

    @Override
    public Set findPhone(ContactType type) {
        throw new OperationNotSupported();
    }

    public Set findByValueStart(String valueStart) {
        return networkFileRepository.findByValueStart(valueStart);
    }

    public Set findByName(String namePart) {
        return networkFileRepository.findByName(namePart);
    }

    @Override
    public void deletedByValue(String value) {
        throw new OperationNotSupported();
    }

}
