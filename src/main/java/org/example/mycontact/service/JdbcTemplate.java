package org.example.mycontact.service;

import lombok.RequiredArgsConstructor;
import org.example.mycontact.person.ContactType;
import org.example.mycontact.utillits.RowMapper;

import javax.sql.DataSource;
import java.sql.*;
import java.util.ArrayList;
import java.util.List;

@RequiredArgsConstructor
public class JdbcTemplate {
    private final DataSource dataSource;


    public <T> List<T> query(String query, RowMapper<T> mapper) {
        List<T> list = new ArrayList<>();
        try (Connection connection = dataSource.getConnection()) {
            Statement statement = connection.createStatement();
            ResultSet resultSet = statement.executeQuery(query);
            while (resultSet.next()) {
                list.add(mapper.map(resultSet));
            }
            return list;
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return list;
    }

    public <T> List<T> queryParams(String query, Object[] params, RowMapper<T> mapper) {
        try (Connection connection = dataSource.getConnection()) {
            PreparedStatement statement = connection.prepareStatement(query);
            for (int i = 0; i < params.length; i++) {
                if (params[i] == ContactType.EMAIL) {
                    String email = "email";
                    params[i] = email;
                }
                if (params[i] == ContactType.PHONE) {
                    String number = "number";
                    params[i] = number;
                }
                statement.setObject(i + 1, params[i]);
            }
            ResultSet resultSet = statement.executeQuery();
            List<T> list = new ArrayList<>(resultSet.getFetchSize());
            while (resultSet.next()) {
                list.add(mapper.map(resultSet));
            }
            return list;
        } catch (Exception e) {
            throw new RuntimeException(e);
        }
    }

    public void update(String query, Object[] params) {
        try (Connection connection = dataSource.getConnection()) {
            PreparedStatement statement = connection.prepareStatement(query);
            for (int i = 0; i < params.length; i++) {
                statement.setObject(i + 1, params[i]);
            }
            statement.execute();
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

}
