package org.example.mycontact.service.api;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.zaxxer.hikari.HikariConfig;
import com.zaxxer.hikari.HikariDataSource;
import lombok.SneakyThrows;
import org.example.mycontact.dto.User;

import javax.servlet.ServletException;
import javax.servlet.ServletOutputStream;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.sql.DataSource;
import java.io.IOException;
import java.sql.*;
import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;


@WebServlet(urlPatterns = {"/", "/add", "/show", "/delete"}, name = "ServletDatabase")
public class ServletDatabase extends HttpServlet {

    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        if (req.getServletPath().equals("/show")) {
            resp.setHeader("Content-Type", "application/json");
            ObjectMapper objectMapper = new ObjectMapper();
            List<User> users = getUser();
            ServletOutputStream out = resp.getOutputStream();
            objectMapper.writeValue(out, users);
        } else {
            super.doGet(req, resp);
        }
    }

    @Override
    protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        if (req.getServletPath().equals("/delete")) {
            resp.setContentType("application/json");
            List<String> login = req.getReader().readLine().lines().collect(Collectors.toList());
            update("DELETE FROM users WHERE login LIKE ?",
                    new Object[]{login.get(0)});
        } else if (req.getServletPath().equals("/add")) {
            resp.setContentType("application/json");
            ObjectMapper objectMapper = new ObjectMapper();
            org.example.mycontact.dto.User user = objectMapper.readValue(req.getReader(), User.class);
            update("INSERT INTO users (login,password,date_born) VALUES(?,?,?) ",
                    new Object[]{user.getLogin(), user.getPassword(), user.getDate_born()});
        } else {
            super.doGet(req, resp);
        }
    }

    @SneakyThrows
    private DataSource createDataSource() {
        HikariConfig config = new HikariConfig();
        Class.forName("org.postgresql.Driver");
        config.setUsername("postgres");
        config.setPassword("skillet17");
        config.setJdbcUrl("jdbc:postgresql://localhost:5432/PhoneBook");
        return new HikariDataSource(config);
    }

    public List<User> getUser() {
        DataSource dataSource = createDataSource();
        try (Connection connection = dataSource.getConnection()) {
            Statement stmt = connection.createStatement();
            ResultSet resultSet = stmt.executeQuery("SELECT * FROM users");
            List<User> users = new ArrayList<>(resultSet.getFetchDirection());
            while (resultSet.next()) {
                users.add(User.builder()
                        .login(resultSet.getString("login"))
                        .password(resultSet.getString("password"))
                        .date_born(resultSet.getString("date_born"))
                        .build()
                );
            }
            return users;
        } catch (SQLException e) {
            return List.of();
        }
    }

    public void update(String query, Object[] params) {
        DataSource dataSource = createDataSource();
        try (Connection connection = dataSource.getConnection()) {
            PreparedStatement statement = connection.prepareStatement(query);
            for (int i = 0; i < params.length; i++) {
                statement.setObject(i + 1, params[i]);
            }
            statement.execute();
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }
}
