package org.example.mycontact.service;

import lombok.AllArgsConstructor;
import org.example.mycontact.exception.ContactNotFoundException;
import org.example.mycontact.exception.DuplicateContactKeyException;
import org.example.mycontact.exception.ValidateExeption;
import org.example.mycontact.person.Contact;
import org.example.mycontact.person.ContactType;
import org.example.mycontact.pesistance.ContactRepository;
import org.example.mycontact.utillits.Validator;

import java.util.Set;

@AllArgsConstructor
public class FileContactService implements ContactService<Contact>{

    private final ContactRepository<Contact> contactRepository;
    private final Validator<Contact> contactValidator;


    @Override
    public void save(Contact contact) {
        if(!contactValidator.isValid(contact)){
            throw new ValidateExeption("Контакт введен не верно");
        }
        if (contactRepository.findByValue(contact.getValue()).isPresent()) {
            throw new DuplicateContactKeyException();
        }
        contactRepository.save(contact);
    }
    @Override
    public Set<Contact> findAll() {
        return contactRepository.sortBySurname();
    }
    @Override
    public Set<Contact> findEmail(ContactType type) {
        return contactRepository.findByType(ContactType.EMAIL);
    }
    @Override
    public Set<Contact> findPhone(ContactType type) {
        return contactRepository.findByType(ContactType.PHONE);
    }
    @Override
    public Set<Contact> findByValueStart(String valueStart) {
        return contactRepository.findByValueStart(valueStart);
    }
    @Override
    public Set<Contact> findByName(String namePart) {
        return contactRepository.findByName(namePart);
    }
    @Override
    public void deletedByValue(String value) {
        contactRepository.findByValue(value)
                .orElseThrow(ContactNotFoundException::new);
        contactRepository.deletedByValue(value);
    }
}

