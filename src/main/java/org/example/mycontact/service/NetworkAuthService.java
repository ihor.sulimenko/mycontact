package org.example.mycontact.service;

import com.google.gson.Gson;
import lombok.Getter;
import lombok.RequiredArgsConstructor;
import org.example.mycontact.dto.User;
import org.example.mycontact.exception.AuthorizationException;

import java.io.IOException;
import java.net.URI;
import java.net.http.HttpClient;
import java.net.http.HttpRequest;
import java.net.http.HttpResponse;
import java.util.List;

@RequiredArgsConstructor
public class NetworkAuthService implements AuthService {
    @Getter
    private String token;
    private final HttpClient httpClient;


    @Override
    public void login(String login, String password) {
        User user = new User(login, password);
        Gson gson = new Gson();
        String newUser = gson.toJson(user);
        try {
            HttpRequest httpRequest = HttpRequest.newBuilder()
                    .POST(HttpRequest.BodyPublishers.ofString(newUser))
                    .header("Accept",
                            "application/json")
                    .header("Content-Type","application/json")
                    .uri(URI.create("https://mag-contacts-api.herokuapp.com/login"))
                    .build();
            HttpResponse<String> response = httpClient.send(httpRequest, HttpResponse.BodyHandlers.ofString());
            if(!response.body().contains("ok")){
                System.out.println(response.body());
                throw new AuthorizationException();
            }
            String[] responseLogin = response.body().split(",");
            for (String s : responseLogin) {
                List<String > value = List.of(s.split(":"));
                token=value.get(1).replace("\"","");
                break;
            }
        } catch (IOException | InterruptedException e) {
            throw new RuntimeException(e);
        }
    }

    @Override
    public void register(String login, String password, String dateBorn) {
        User user = new User(login, password, dateBorn);
        Gson gson = new Gson();
        String newUser = gson.toJson(user);
        try {
            HttpRequest httpRequest = HttpRequest.newBuilder()
                    .POST(HttpRequest.BodyPublishers.ofString(newUser))
                    .header("Accept",
                            "application/json")
                    .header("Content-Type","application/json")
                    .uri(URI.create("https://mag-contacts-api.herokuapp.com/register"))
                    .build();
            HttpResponse<String> response = httpClient.send(httpRequest, HttpResponse.BodyHandlers.ofString());
            System.out.println(response.body());
            if(response.statusCode()!=200){
                throw new AuthorizationException();
            }
        } catch (IOException | InterruptedException e) {
            throw new RuntimeException(e);
        }
    }

    @Override
    public boolean isAuth() {
        return token != null;
    }
}
