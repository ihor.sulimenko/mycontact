package org.example.mycontact.menu;

import com.google.gson.Gson;
import org.example.mycontact.dto.User;
import org.example.mycontact.exception.AuthorizationException;
import org.junit.jupiter.api.Test;

import java.io.IOException;
import java.net.URI;
import java.net.http.HttpClient;
import java.net.http.HttpRequest;
import java.net.http.HttpResponse;
import java.util.List;

class ContactTest {
    @Test
    public void testNetwork(){
        HttpClient httpClient = HttpClient.newBuilder().build();
        User user = new User("login1234", "1234", "1995-02-12");
        Gson gson = new Gson();
        String newUser = gson.toJson(user);
        try {
            HttpRequest httpRequest = HttpRequest.newBuilder()
                    .POST(HttpRequest.BodyPublishers.ofString(newUser))
                    .header("Accept",
                            "application/json")
                    .header("Content-Type","application/json")
                    .uri(URI.create("https://mag-contacts-api.herokuapp.com/register"))
                    .build();
            HttpResponse<String> response = httpClient.send(httpRequest, HttpResponse.BodyHandlers.ofString());
            System.out.println(response.statusCode());
            System.out.println(response.request());
            System.out.println(response.body());
            System.out.println(response.headers());


        } catch (IOException | InterruptedException e) {
            throw new RuntimeException(e);
        }
    }
    @Test
    public void login() {
        String token = null;
        HttpClient httpClient = HttpClient.newBuilder().build();
        User user = new User("login1234", "1234");
        Gson gson = new Gson();
        String newUser = gson.toJson(user);
        try {
            HttpRequest httpRequest = HttpRequest.newBuilder()
                    .POST(HttpRequest.BodyPublishers.ofString(newUser))
                    .header("Accept",
                            "application/json")
                    .header("Content-Type","application/json")
                    .uri(URI.create("https://mag-contacts-api.herokuapp.com/login"))
                    .build();
            HttpResponse<String> response = httpClient.send(httpRequest, HttpResponse.BodyHandlers.ofString());
            if(!response.body().contains("ok")){
                throw new AuthorizationException();
            }
                String[] responseLogin = response.body().split(",");
                for (String s : responseLogin) {
                    List<String > value = List.of(s.split(":"));
                    token=value.get(1);
                    System.out.println(token);
                    break;
            }
        } catch (IOException | InterruptedException e) {
            throw new RuntimeException(e);
        }
    }
    @Test
    public void login123(){
        String login = "\"token\":\"eyJhbGciOiJIUzI1NiJ9.eyJzdWIiOiJsb2dpbjEyMzQiLCJpYXQiOjE2NDE5OTAxNzAsImV4cCI6MTY0MTk5Mzc3MH0.f0_JVbQZNl8XJ9cS73rENPQgua-ne2dxL4PiXb0HfyU\",\"status\":\"ok\"";
        String[] log = login.split(",");
        for (String s : log) {
            List<String > value = List.of(s.split(":"));
            break;
        }

    }
//    @Test
//    public void testToGson(){
//        View view = new ViewContacts();
//        Validator<HumanContact> validator = new ContactValidator(List.of(new EmailValidator(),new PhoneValidator()));
//        ContactService contactService = new ContactService(validator,view);
//        HumanContact humanContact = new HumanContact(
//                "иван",
//                "иванов",
//                "иванович",
//
//                TypeContacts.EMAIL,
//                "email@email.ua");
//        contactService.addToSet(humanContact);
//        FileToGson fileToGson = new FileToGson(contactService);
//        fileToGson.textFile();
//        fileToGson.readTxtFile();
//    }
//    @Test
//    public  void testToTxt(){
//        View view = new ViewContacts();
//        Validator<HumanContact> validator = new ContactValidator(List.of(new EmailValidator(),new PhoneValidator()));
//        ContactService contactService = new ContactService(validator,view);
//        HumanContact humanContact = new HumanContact(
//                "иван",
//                "иванов",
//                "иванович",
//
//                TypeContacts.EMAIL,
//                "email@email.ua");
//        contactService.addToSet(humanContact);
//       FileToTxt fileToTxt = new FileToTxt(contactService);
//       fileToTxt.textFile();
//       fileToTxt.readTxtFile();
//    }
//    @Test
//    public  void testToBite(){
//        View view = new ViewContacts();
//        Validator<HumanContact> validator = new ContactValidator(List.of(new EmailValidator(),new PhoneValidator()));
//        ContactService contactService = new ContactService(validator,view);
//        HumanContact humanContact = new HumanContact(
//                "иван",
//                "иванов",
//                "иванович",
//
//                TypeContacts.EMAIL,
//                "email@email.ua");
//        contactService.addToSet(humanContact);
//       FileToBite fileToBite = new FileToBite(contactService);
////       fileToBite.textFile();
//       fileToBite.readTxtFile();
//    }
//    @Test
//    @DisplayName("Проверка добавления")
//    public void testAddToContact() {
//        View view = new ViewContacts();
//        ContactService contactService = new ContactService(view);
//        HumanContact humanContact = new HumanContact(
//                "иван",
//                "иванов",
//                "иванович",
//
//                TypeContacts.EMAIL,
//                "email@email.ua");
//        contactService.addToSet(humanContact);
//        assertTrue(contactService.getContacts().contains(humanContact));
//        HumanContact humanContact1 = new HumanContact(
//                "вася",
//                "василий",
//                "невасилий",
//
//                TypeContacts.EMAIL,
//                "email@email.com");
//        contactService.addToSet(humanContact1);
//        assertTrue(contactService.getContacts().contains(humanContact1));
//    }
//
//    @Test
//    @DisplayName("Проверка сортировки")
//    public void testSortContact() {
//        View view = new ViewContacts();
//        ContactService contactService = new ContactService(view);
//        HumanContact humanContact = new HumanContact(
//                "иван",
//                "иванов",
//                "иванович",
//
//                TypeContacts.EMAIL,
//                "email@email.ua");
//        contactService.addToSet(humanContact);
//        HumanContact humanContact1 = new HumanContact(
//                "вася",
//                "василий",
//                "невасилий",
//
//                TypeContacts.EMAIL,
//                "email@email.com");
//        contactService.addToSet(humanContact1);
////        contactService.sortEmailBySurname();
//
//    }
//
//    @Test
//    @DisplayName("Проверка только по типу")
//    public void testOnlyByType() {
//
//    }
//
//    @Test
//    @DisplayName("Проверка поиску начала контакта Ф.И.О.")
//    public void testSearchByFullName() {
//    }
//
//
//    @Test
//    @DisplayName("Проверка поиску по типу контакта ")
//    public void testSearchByType() {
//
//    }
//
//    @Test
//    @DisplayName("Проверка удаления контакта ")
//    public void testDeletedContact() {
//        View view = new ViewContacts();
//        ContactService contactService = new ContactService(view);
//        HumanContact humanContact = new HumanContact(
//                "вася",
//                "василий",
//                "невасилий",
//                TypeContacts.EMAIL,
//                "email@email.com");
//        contactService.addToSet(humanContact);
//        contactService.deletedContactByValue("email@email.com");
//        assertFalse(contactService.getContacts().contains(humanContact));
//    }

}